'use strict';

const R = require('rethinkdb');
const fs = require('fs');

exports.register = function (server, options, next) {
  fs.readFile(options.caCert, (err, caCert) => {
    if (err) server.log(err);
    R.connect({
      db: options.dbName,
      host: options.host,
      port: options.port,
      authKey: options.authKey,
      ssl: {
        ca: caCert
      }
    }, (err, conn) => {

      if (err) {
        return next(err);
      }

      server.app.db = conn;
      next();
    });
  })
};

exports.register.attributes = {
  pkg: require('./package')
};
