const R = require('rethinkdb');
const ENABLE_NOTIFS = process.env.NOTIFS

module.exports = function (server) {
  return server.method([
    {
      name: 'notifs.sendEmailConfirmation',
      method: function (user, transporter) {
        let token = server.methods.auth.createEmailToken(user)
        let mailOptions = {
          from: '"Buscando Huellas" <no-reply@buscandohuellas.com>',
          subject: 'Confirma tu Email',
          to: user.email,
          html: `
            <div style="text-align: center">
              <h2>Por favor confirma tu Email</h2>
              <p>
                Haz click en el botón de abajo para confirmar tu Email y poder empezar a usar la plataforma:
              </p>
              <a href="https://api.buscandohuellas.com/confirm_email?token=${token}"
              style="background-color: #72bb46; padding: 15px; border-radius: 5px; display: inline-block; text-decoration: none; font-weight: 700; color: white;">
                CONFIRMAR EMAIL
              </a>
            </div>
          `
        }
        transporter.sendMail(mailOptions)
      }
    },
    {
      name: 'notifs.sendExpire',
      method: function (data, transporter) {
        server.methods.db.search.getById(data, (err, result) => {
          var offset = 0
          let mailOptions = {
            from: '"Buscando Huellas" <no-reply@buscandohuellas.com>',
            subject: 'Renueva tu búsqueda',
            to: [ result.user.email ],
            html: `
            <div style="text-align: center">
              <h2>Tu búsqueda "${result.pet_name}-${result.title}" expiró</h2>
              <p>Si sigues en la búsqueda, por favor renuevala.</p>
              <p>Para renovarla, ingresa a la plataforma y haz click en el
              botón "Renovar" dentro del panel "Mis Búsquedas"
              </p>
              <a href="https://buscandohuellas.com/usersearches"
              style="background-color: #72bb46; padding: 15px; border-radius: 5px; display: inline-block; text-decoration: none; font-weight: 700; color: white;">
                IR A MI PANEL
              </a>
            </div>
          `
          }
          setTimeout(() => {
            transporter.sendMail(mailOptions);
          }, 100 + offset)
          offset += 100
        })
      }
    },
    {
      name: 'notifs.sendFound',
      method: function (data, transporter) {
        var offset = 0
        let mailOptions = {
          from: '"Buscando Huellas" <no-reply@buscandohuellas.com>',
          subject: 'Se produjo un nuevo reencuentro',
          to: "notif@buscandohuellas.com",
          html: `
            <div style="text-align: center">
              <h2>Se produjo un nuevo reencuentro</h2>
              <p>Se produjo un nuevo reencuentro, click en el botón para ver
                la publicación.
              </p>
              <a href="https://buscandohuellas.com/search?search_id=${data}"
              style="background-color: #72bb46; padding: 15px; border-radius: 5px; display: inline-block; text-decoration: none; font-weight: 700; color: white;">
                VER PUBLICACIÓN
              </a>
            </div>
          `
        }
        setTimeout(() => {
          transporter.sendMail(mailOptions);
        }, 100 + offset)
        offset += 100
      }
    },
    {
      name: 'notifs.markMatches',
      method: function (data) {
        server.methods.db.search.findMatches(data.id, (err, results) => {
          if (err) console.log(err);
          results.map((result) => {
            R.table('searches')
              .get(result.doc.id)
              .update({ notify: true })
              .run(server.app.db)
          })
        })
      }
    },
    {
      name: 'notifs.notifyMatches',
      method: function (transporter) {
        R.table('searches')
          .getAll(true, { index: 'notify' })
          .merge((search) => {
            return {
              user: R.table('users').get(search('user_id'))
            }
          })
          .run(server.app.db, (err, results) => {
            var offset = 0
            results.each((err, result) => {
              if (ENABLE_NOTIFS) {
                R.table('searches').get(result.id).update({ notify: false }).run(server.app.db)
                let link_url = `https://buscandohuellas.com/searchmatches?search_id=${result.id}`
                // setup email data with unicode symbols
                let mailOptions = {
                  from: '"Buscando Huellas" <no-reply@buscandohuellas.com>',
                  subject: 'Hay nuevos resultados para tu búsqueda',
                  to: [ result.user.email ],
                  html: `
            <div style="text-align: center">
              <h2>Nuevos resultados para tu búsqueda: ${result.title}</h2>
              <p>Se ha publicado una búsqueda que coincide con la tuya, ingresa <br/>
              a la plataforma y revísala haciendo click en el siguiente botón:
              </p>
              <a href="https://buscandohuellas.com/searchmatches?search_id=${result.id}"
              style="background-color: #72bb46; padding: 15px; margin: 20px; border-radius: 5px; display: inline-block; text-decoration: none; font-weight: 700; color: white;">
                VER NUEVOS RESULTADOS
              </a>
              <p>
              Nota: Si ya ingresaste a la plataforma horas antes, probablemente ya has visto y descartado
              los nuevos resultados ya que las notificaciones demoran algunas horas en llegar. Si al ingresar ahora
              no puedes ver nuevos resultados, no te preocupes, significa que ya los viste horas antes. Todo esta
              funcionando bien. Mientras tanto, estamos trabajando en un mejor sistema de notificaciones.
              </p>
            </div>
          `
                };
                // send mail with defined transport object
                setTimeout(() => {
                  transporter.sendMail(mailOptions);
                }, 100 + offset)
                offset += 100
              }
            })
          })
      }
    },
    {
      name: 'notifs.markVolunteers',
      method: function (data) {
        server.methods.db.search.findVolunteerMatches(data.id, (err, results) => {
          if (err) console.log(err)
          results.map((result) => {
            R.table('users')
              .get(result.doc.id)
              .update({ volunteerData: { notify: true } })
              .run(server.app.db)
          })
        })
      }
    },
    {
      name: 'notifs.notifyVolunteersEmergency',
      method: function (data, transporter) {
        server.methods.db.search.findVolunteerMatches(data.id, (err, results) => {
          if (err) console.log(err)
          var offset = 0
          results.map((result) => {
            let user = result.doc
            if (ENABLE_NOTIFS) {
              // setup email data with unicode symbols
              let mailOptions = {
                from: '"Buscando Huellas" <no-reply@buscandohuellas.com>',
                subject: 'Voluntario: Hay una nueva EMERGENCIA en tu zona',
                to: [ user.email ],
                html: `
            <div style="text-align: center">
              <h2>Voluntario: Hay una nueva EMERGENCIA en tu zona</h2>
              <p>Ingresa a la plataforma y revísa la publicación haciendo click en el siguiente botón:
              </p>
              <a href="https://buscandohuellas.com/search?search_id=${data.id}"
              style="background-color: #72bb46; padding: 15px; border-radius: 5px; display: inline-block; text-decoration: none; font-weight: 700; color: white;">
                VER PUBLICACIÓN
              </a>
            </div>
          `
              };
              // send mail with defined transport object
              setTimeout(() => {
                transporter.sendMail(mailOptions);
              }, 100 + offset)
              offset += 100
            }
          })
        })
      }
    },
    {
      name: 'notifs.notifyVolunteers',
      method: function (transporter, callback) {
        R.table('users')
          .getAll(true, { index: 'notifyVolunteer' })
          .run(server.app.db, (err, results) => {
            if (err) console.log(err)
            var offset = 0
            results.each((err, result) => {
              let user = result
              R.table('users').get(user.id).update({ volunteerData: { notify: false } }).run(server.app.db)
              if (ENABLE_NOTIFS) {
                // setup email data with unicode symbols
                let mailOptions = {
                  from: '"Buscando Huellas" <no-reply@buscandohuellas.com>',
                  subject: 'Voluntario: Hay nuevas publicaciones en tu zona',
                  to: [ user.email ],
                  html: `
            <div style="text-align: center">
              <h2>Voluntario: Hay nuevas publicaciones en tu zona</h2>
              <p>Ingresa a la plataforma y revísa las publicaciónes haciendo click en el siguiente botón:
              </p>
              <a href="https://buscandohuellas.com/myzone"
              style="background-color: #72bb46; padding: 15px; border-radius: 5px; display: inline-block; text-decoration: none; font-weight: 700; color: white;">
                VER MI ZONA
              </a>
              <p>
              Nota: Si ya ingresaste a la plataforma horas antes, probablemente ya has visto y descartado
              los nuevos resultados ya que las notificaciones demoran algunas horas en llegar. Si al ingresar ahora
              no puedes ver nuevos resultados, no te preocupes, significa que ya los viste horas antes. Todo esta
              funcionando bien. Mientras tanto, estamos trabajando en un mejor sistema de notificaciones.
              </p>
            </div>
          `
                };
                // send mail with defined transport object
                setTimeout(() => {
                  transporter.sendMail(mailOptions);
                }, 100 + offset)
                offset += 100
              }
            })
          })
      }
    }
  ])
}