'use strict';
const nodemailer = require('nodemailer')
const Wreck = require('wreck')
const methods = require('./methods')

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
  host: 'email-smtp.us-west-2.amazonaws.com',
  port: 465,
  secure: true, // secure:true for port 465, secure:false for port 587
  auth: {
    user: process.env.AMAZON_SES_USER,
    pass: process.env.AMAZON_SES_PASS
  }
});

exports.register = function (server, options, next) {

  methods(server)

  server.events.on('expireSearch', (search) => {
    server.methods.notifs.sendExpire(search, transporter)
  })

  server.events.on('searchFound', (search) => {
    server.methods.notifs.sendFound(search, transporter)
  })

  server.events.on('newSearch', (search) => {
    if (search.category == 'emergency') {
      server.methods.notifs.notifyVolunteersEmergency(search, transporter)
    } else {
      server.methods.notifs.markMatches(search)
      server.methods.notifs.markVolunteers(search)
    }
  })

  server.events.on('sendEmailConfirmation', (user) => {
    server.methods.notifs.sendEmailConfirmation(user, transporter)
  })



  server.route({
    method: 'POST',
    path: '/send_notifications',
    config: {
      auth: false
    },
    handler: function (request, reply) {
      if (request.payload.pass == 'Tacuari182') {
        server.methods.notifs.notifyMatches(transporter)
        server.methods.notifs.notifyVolunteers(transporter)
        reply('sending')
      } else {
        reply('not authorized')
      }
    }
  })

  next();
};

exports.register.attributes = {
  pkg: require('./package')
};
