const jwt = require('jsonwebtoken')

const SECRET_KEY = process.env.BH_SECRET_KEY || 'secret_key'

module.exports = function (server) {
  server.method([
    {
      name: 'auth.validateToken',
      method: function (decoded, request, callback) {
        // do your checks to see if the person is valid
        server.methods.db.user.getById(decoded.id, (err, result) => {
          if (decoded.id == result.id) {
            return callback(null, true)
          } else {
            return callback(null, false)
          }
        })
      }
    },
    {
      name: 'auth.createToken',
      method: function (user) {
        return jwt.sign({ id: user.id }, SECRET_KEY, { algorithm: 'HS256', expiresIn: "365d" });
      }
    },
    {
      name: 'auth.createEmailToken',
      method: function (user) {
        return jwt.sign({ id: user.id, email: user.email }, SECRET_KEY, { algorithm: 'HS256', expiresIn: "10d" })
      }
    },
    {
      name: 'auth.confirmEmail',
      method: function (token, callback) {
        const decoded = jwt.verify(token, SECRET_KEY)
        server.methods.db.user.getById(decoded.id, (err, result) => {
          if (err) callback(err)
          if (result) {
            if (result.email == decoded.email) {
              server.methods.db.user.setEmailConfirmed(result.id, (err, result) => {
                if (err) callback(err)
                callback(null, true)
              })
            } else {
              callback(null, false)
            }
          } else {
            callback(null, false)
          }
        })
      }
    }
  ])
}