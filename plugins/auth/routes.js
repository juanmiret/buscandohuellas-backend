const Wreck = require('wreck')
const Boom = require('boom')
const bcrypt = require('bcrypt')

module.exports = function (server) {
  server.route([
    {
      method: 'POST',
      path: '/resend_confirmation',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        server.methods.db.user.findByEmail(request.payload.email, (err, results) => {
          if (err) reply(err)
          if (results[ 0 ]) {
            server.events.emit('sendEmailConfirmation', results[ 0 ])
            reply({ success: true })
          } else {
            reply(Boom.forbidden('No hay una cuenta existente con este email.'))
          }
        })
      }
    },
    {
      method: 'POST',
      path: '/update_account_email',
      handler: function (request, reply) {
        server.methods.db.user.findByEmail(request.payload.email, (err, results) => {
          if (results[ 0 ]) {
            reply(Boom.forbidden('Este email ya esta en uso.'))
          } else {
            server.methods.db.user.updateEmail(request.auth.credentials.id, request.payload.email, (err, result) => {
              if (err) reply(err)
              server.events.emit('sendEmailConfirmation', result)
              reply({ updated: true })
            })
          }
        })
      }
    },
    {
      method: 'GET',
      path: '/confirm_email',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        server.methods.auth.confirmEmail(request.query.token, (err, confirmed) => {
          if (err) reply(err)
          if (confirmed) {
            reply.redirect('https://buscandohuellas.com/login?email_confirmed=true')
          } else {
            reply('Hubo un error al confirmar el email, por favor intente de nuevo o comuniquese con nosotros a contacto@buscandohuellas.com')
          }
        })
      }
    },
    {
      method: 'POST',
      path: '/create_account_email',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        let user = request.payload
        server.methods.db.user.findByEmail(user.email, (err, results) => {
          if (err) reply(err);
          if (results[ 0 ]) {
            reply(Boom.forbidden('Ya existe una cuenta con este email'))
          } else {
            server.methods.db.user.createWithEmail(user, (err, result) => {
              if (err) reply(err)
              if (result) {
                server.events.emit('sendEmailConfirmation', result)
                reply({ created: true })
              }
            })
          }
        })
      }
    },
    {
      method: 'POST',
      path: '/login_with_email',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        server.methods.db.user.findByEmail(request.payload.email, (err, results) => {
          if (err) reply(err)
          let storedUser = results[ 0 ]
          if (storedUser) {
            bcrypt.compare(request.payload.password, storedUser.password, (err, correct) => {
              if (correct) {
                reply({
                  token: server.methods.auth.createToken(storedUser)
                })
              } else {
                reply(Boom.forbidden('La contraseña ingresada no es correcta.'))
              }
            })
          } else {
            reply(Boom.forbidden('El email ingresado no es válido.'))
          }
        })
      }
    },
    {
      method: 'POST',
      path: '/loginFacebook',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        let accessToken = request.payload.profile.accessToken
        Wreck.get('https://graph.facebook.com/me?fields=id,name,email&access_token=' + accessToken, (err, res, payload) => {
          if (err) reply(err)
          let profile = JSON.parse(payload)
          server.methods.db.user.findOrCreate({
            name: profile.name,
            email: profile.email,
            login_type: 'facebook',
            facebook_id: profile.id,
            profile_picture: 'https://graph.facebook.com/' + profile.id + '/picture?type=square&width=300&height=300'
          }, (err, result) => {
            if (err) {
              reply(err)
            };
            reply({
              token: server.methods.auth.createToken(result)
            })
          })
        })
      }
    },
    {
      method: 'POST',
      path: '/loginGoogle',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        let accessToken = request.payload.google_token
        Wreck.get('https://www.googleapis.com/plus/v1/people/me?access_token=' + accessToken + '&key=365252207594-rds9c1ala8aeeuun2qbq5fipog5n2dk9.apps.googleusercontent.com',
          (err, res, payload) => {
            if (err) reply(err)
            let profile = JSON.parse(payload)
            let primaryEmail = ''
            profile.emails.map((email) => {
              if (email.type == 'account') {
                primaryEmail = email.value
              }
            })
            server.methods.db.user.findOrCreate({
              name: profile.displayName,
              email: primaryEmail,
              login_type: 'google',
              google_id: profile.id,
              profile_picture: profile.image.url
            }, (err, result) => {
              if (err) {
                reply(err)
              };
              reply({
                token: server.methods.auth.createToken(result)
              })
            })
          })
      }
    }
  ])
}