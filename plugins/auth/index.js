'use strict';

const hapiJwt = require('hapi-auth-jwt2')
const Boom = require('boom')
const routes = require('./routes')
const methods = require('./methods')

const SECRET_KEY = process.env.BH_SECRET_KEY || 'secret_key'

exports.register = function (server, options, next) {

  methods(server)

  server.register(hapiJwt)
  server.auth.strategy('jwt', 'jwt',
    {
      key: SECRET_KEY,          // Never Share your secret key
      validateFunc: server.methods.auth.validateToken,            // validate function defined above
      verifyOptions: { algorithms: [ 'HS256' ] } // pick a strong algorithm
    });

  server.auth.default('jwt');

  routes(server)
  next();

};

exports.register.attributes = {
  pkg: require('./package')
};