'use strict';

const Wreck = require('wreck')

const routes = require('./routes')
const db_methods = require('./db_methods')

const after = function (server, next) {
  db_methods(server)
  routes(server)
  next();
};

exports.register = function (server, options, next) {
  server.dependency([ 'bh-database' ], after);
  next();
};

exports.register.attributes = {
  pkg: require('./package')
};
