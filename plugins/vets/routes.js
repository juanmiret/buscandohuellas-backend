const Wreck = require('wreck')

module.exports = function (server) {
  return server.route([
    {
      method: 'POST',
      path: '/vets',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        let { location, query } = request.payload
        server.methods.db.vets.getActive(location, (err, results) => {
          if (err) reply(err);
          reply(results)
        })
      }
    },
    {
      method: 'POST',
      path: '/vet/{id}/delete',
      handler: function (request, reply) {
        server.methods.db.vet.delete(request.params.id, request.auth.credentials.id, (err, result) => {
          if (err) reply(err);
          if (result.errors) {
            reply(result.errors).code(400)
          } else {
            reply()
          }
        })
      }
    },
    {
      method: 'GET',
      path: '/vet/{id}',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        server.methods.db.vet.getById(request.params.id, (err, result) => {
          if (err) reply(err);
          reply(result)
        })
      }
    },
    {
      method: 'POST',
      path: '/vet/add',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        let vet_data = request.payload.vet_data
        server.methods.db.vet.create(vet_data, (err, result) => {
          if (err) reply(err);
          Wreck.put(`https://api.uploadcare.com/groups/${result.gallery.uuid}/storage/`, {
            headers: {
              'Authorization': `Uploadcare.Simple ${process.env.UPLOADCARE_PUBLIC_KEY}:${process.env.UPLOADCARE_SECRET_KEY}`
            }
          }, (err, res, payload) => {
          })
          reply(result)
        })
      }
    }
  ])
}