const R = require('rethinkdb')

module.exports = function (server) {
  return server.method([
    {
      name: 'db.vet.getById',
      method: function (id, callback) {
        R.table('vets')
          .get(id)
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.vet.create',
      method: function (vet_data, callback) {
        vet_data.category = 'vet'
        vet_data.location = R.point(vet_data.lng, vet_data.lat)
        vet_data.created_at = R.now()
        vet_data.active = false
        R.table('vets')
          .insert(vet_data)
          .run(server.app.db, (err, result) => {
            if (err) callback(err);
            server.methods.db.vet.getById(result.generated_keys[ 0 ], callback)
          })
      }
    },
    {
      name: 'db.vets.getActive',
      method: function (location, callback) {
        location = R.point(location.lng, location.lat)
        R.table('vets')
          .getNearest(location, { index: 'location', maxDist: 50, unit: 'km', maxResults: 20000 })
          .filter({ doc: { active: true } })
          .run(server.app.db, callback)
      }
    }
  ])
}