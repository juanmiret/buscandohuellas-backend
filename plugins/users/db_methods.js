const R = require('rethinkdb');
const bcrypt = require('bcrypt')


module.exports = function (server) {
  return server.method([
    {
      name: 'db.user.updateEmail',
      method: function (user_id, newEmail, callback) {
        R.table('users')
          .get(user_id)
          .update({
            email: newEmail,
            email_confirmed: false,
            email_updated_at: R.now()
          }, { returnChanges: true })
          .run(server.app.db, (err, result) => {
            callback(err, result.changes[ 0 ].new_val)
          })
      }
    },
    {
      name: 'db.user.createWithEmail',
      method: function (user, callback) {
        let newUser = {
          name: user.name,
          email: user.email,
          created_at: R.now(),
          profile_picture: 'https://ucarecdn.com/a36043f8-3136-4da0-8c19-73e1a942217c/-/resize/100x100/',
          login_type: 'email',
          email_confirmed: false
        }
        bcrypt.hash(user.password, 8, function (err, hash) {
          if (err) callback(err)
          newUser.password = hash
          R.table('users')
            .insert(newUser, { returnChanges: true })
            .run(server.app.db, (err, result) => {
              callback(err, result.changes[ 0 ].new_val)
            })
        })
      }
    },
    {
      name: 'db.user.createUser',
      method: function (profile, callback) {
        profile.created_at = R.now()
        R.table('users')
          .insert(profile, { returnChanges: true })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.user.findByEmail',
      method: function (email, callback) {
        R.table('users')
          .getAll(email, { index: 'email' }).limit(1)
          .coerceTo('array')
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.user.getById',
      method: function (id, callback) {
        R.table('users')
          .get(id).without('password')
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.user.getByFacebookId',
      method: function (facebook_id, callback) {
        R.table('users')
          .getAll(facebook_id, { index: 'facebook_id' }).limit(1)
          .coerceTo('array')
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.user.findOrCreate',
      method: function (profile, callback) {
        if (profile.facebook_id) {
          profile.email = profile.email || null
          profile.email ? profile.email_confirmed = true : profile.email_confirmed = false
          server.methods.db.user.getByFacebookId(profile.facebook_id, (err, results) => {
            if (err) callback(err)
            let storedUser = results[ 0 ]
            if (storedUser) {
              storedUser.last_login = R.now()
              storedUser.login_type = 'facebook'
              storedUser.name = profile.name
              R.table('users')
                .get(storedUser.id)
                .update(storedUser, { returnChanges: true })
                .run(server.app.db, (err, result) => {
                  callback(err, result.changes[ 0 ].new_val)
                })
            } else {
              server.methods.db.user.createUser(profile, (err, result) => {
                callback(err, result.changes[ 0 ].new_val)
              })
            }
          })
        } else if (profile.email) {
          profile.email_confirmed = true
          server.methods.db.user.findByEmail(profile.email, (err, results) => {
            if (err) callback(err)
            let storedUser = results[ 0 ]
            if (storedUser) {
              profile.last_login = R.now()
              R.table('users')
                .get(storedUser.id)
                .update(profile, { returnChanges: true })
                .run(server.app.db, (err, result) => {
                  callback(err, result.changes[ 0 ].new_val)
                })
            } else {
              server.methods.db.user.createUser(profile, (err, result) => {
                callback(err, result.changes[ 0 ].new_val)
              })
            }
          })
        }
      }
    },
    {
      name: 'db.user.getSearches',
      method: function (user_id, callback) {
        R.table('searches')
          .getAll(user_id, { index: 'user_id' })
          .filter({ active: true })
          .coerceTo('array')
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.user.setVolunteer',
      method: function (user_id, volunteerData, callback) {
        R.table('users')
          .get(user_id)
          .update({
            isVolunteer: true,
            volunteerData: {
              address: volunteerData.address,
              lat: volunteerData.lat,
              lng: volunteerData.lng,
              radius: parseFloat(volunteerData.radius),
              location: R.point(volunteerData.lng, volunteerData.lat),
              categories: volunteerData.categories
            }
          }, { non_atomic: true })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.user.unsetVolunteer',
      method: function (user_id, callback) {
        R.table('users')
          .get(user_id)
          .update({
            isVolunteer: false,
            volunteerData: null
          })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.user.setEmailConfirmed',
      method: function (user_id, callback) {
        R.table('users')
          .get(user_id)
          .update({
            email_confirmed: true
          })
          .run(server.app.db, callback)
      }
    }
  ])
}