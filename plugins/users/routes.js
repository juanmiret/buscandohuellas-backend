module.exports = function (server) {
  return server.route([
    {
      method: 'POST',
      path: '/user/set_volunteer',
      handler: function (request, reply) {
        server.methods.db.user.setVolunteer(request.auth.credentials.id, request.payload, (err, result) => {
          if (err) reply(err)
          reply()
        })
      }
    },
    {
      method: 'POST',
      path: '/user/unset_volunteer',
      handler: function (request, reply) {
        server.methods.db.user.unsetVolunteer(request.auth.credentials.id, (err, result) => {
          if (err) reply(err)
          reply()
        })
      }
    },
    {
      method: 'GET',
      path: '/user/my_searches',
      handler: function (request, reply) {
        server.methods.db.user.getSearches(request.auth.credentials.id, (err, results) => {
          if (err) reply(err)
          reply(results)
        })
      }
    },
    {
      method: 'GET',
      path: '/user/volunteerSearches',
      handler: function (request, reply) {
        server.methods.db.searches.findVolunteerSearches(request.auth.credentials.id, (err, results) => {
          if (err) reply(err);
          reply(results)
        })
      }
    },
    {
      method: 'GET',
      path: '/user/get_current',
      handler: function (request, reply) {
        server.methods.db.user.getById(request.auth.credentials.id, (err, result) => {
          if (err) reply(err);
          reply({
            user: result
          })
        })
      }
    }
  ])
}