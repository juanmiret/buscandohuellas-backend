const R = require('rethinkdb');

module.exports = function (server) {
  return server.method([
    {
      name: 'db.search.renew',
      method: function (search_id, callback) {
        R.table('searches')
          .get(search_id)
          .update({ expires_at: R.now().add(2592000), expired: false })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.searches.expire',
      method: function (callback) {
        R.table('searches')
          .between(R.minval, R.now(), { index: 'expires_at', rightBound: 'closed' })
          .update({ expired: true }, { returnChanges: true })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.search.setFound',
      method: function (search_id, user_id, callback) {
        R.table('searches')
          .get(search_id)
          .update({ found: true })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.searches.getActive',
      method: function (location, query, callback) {
        location = R.point(location.lng, location.lat)
        R.table('searches')
          .getNearest(location, { index: 'location', maxDist: 50, unit: 'km', maxResults: 20000 })
          .filter({ doc: { active: true, expired: false, found: false } })
          .filter({ doc: query })
          .merge((result) => {
            return {
              user: R.table('users').get(result('doc')('user_id'))
            }
          })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.search.delete',
      method: function (search_id, user_id, callback) {
        R.table('searches')
          .get(search_id)
          .update({ active: false })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.search.discard',
      method: function (search_id, discard_id, callback) {
        R.table('searches')
          .get(search_id)
          .update({ discards: R.row('discards').default([]).append(discard_id) })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.search.findMatches',
      method: function (id, callback) {
        server.methods.db.search.getById(id, (err, result) => {
          if (err) server.log(err);
          let matchCategory = result.category === 'lost' ? 'found' : 'lost'
          R.table('searches')
            .getNearest(result.location, { index: 'location', maxDist: 20, unit: 'km', maxResults: 20000 })
            .filter({ doc: { category: matchCategory, expired: false, active: true, found: false } })
            .filter(
            R.row('doc')('specie').eq(result.specie).or(R.row('doc')('specie').eq('undef'))
            )
            .filter(
            R.row('doc')('sex').eq(result.sex).or(R.row('doc')('sex').eq('undef'))
            )
            .filter(
            function (doc) {
              return R.expr(result.discards)
                .contains(doc("doc")("id"))
                .not();
            })
            .merge((result) => {
              return {
                user: R.table('users').get(result('doc')('user_id'))
              }
            })
            .run(server.app.db, (err, cursor) => {
              if (err) server.log(err);
              cursor.toArray(callback)
            })
        })
      }
    },
    {
      name: 'db.search.getById',
      method: function (id, callback) {
        R.table('searches')
          .get(id)
          .merge((result) => {
            return {
              user: R.table('users').get(result('user_id'))
            }
          })
          .run(server.app.db, callback)
      }
    },
    {
      name: 'db.search.create',
      method: function (search_data, callback) {
        search_data.location = R.point(search_data.lng, search_data.lat)
        search_data.created_at = R.now()
        search_data.expires_at = R.now().add(2592000)
        search_data.discards = []
        search_data.active = true
        search_data.expired = false
        search_data.found = false
        R.table('searches')
          .insert(search_data)
          .run(server.app.db, (err, result) => {
            if (err) server.log(err);
            server.methods.db.search.getById(result.generated_keys[ 0 ], callback)
          })
      }
    },
    {
      name: 'db.searches.findVolunteerSearches',
      method: function (user_id, callback) {
        server.methods.db.user.getById(user_id, (err, result) => {
          if (result.isVolunteer) {
            let { location, radius, categories } = result.volunteerData
            R.table('searches')
              .getNearest(location, { index: 'location', maxDist: radius, unit: 'km', maxResults: 20000 })
              .filter({ doc: { active: true, found: false, expired: false } })
              .filter(function (search) {
                return R.expr(categories).contains(search('doc')('category'))
              })
              .merge((result) => {
                return {
                  user: R.table('users').get(result('doc')('user_id'))
                }
              })
              .run(server.app.db, (err, cursor) => {
                if (err) console.log(err)
                cursor.toArray(callback)
              })
          }
        })
      }
    },
    {
      name: 'db.search.findVolunteerMatches',
      method: function (id, callback) {
        server.methods.db.search.getById(id, (err, search) => {
          R.table('users')
            .getNearest(search.location, { index: 'volunteerLocation', maxDist: 20, unit: 'km', maxResults: 200000 })
            .filter(function (user) {
              return user('doc')('volunteerData')('categories').contains(search.category)
            })
            .filter(function (user) {
              return user('doc')('volunteerData')('location').distance(search.location, { unit: 'km' }).le(user('doc')('volunteerData')('radius'))
            })
            .run(server.app.db, (err, cursor) => {
              cursor.toArray(callback)
            })
        })
      }
    }
  ])
}