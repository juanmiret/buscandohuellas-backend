const Wreck = require('wreck')

module.exports = function (server) {
  return server.route([
    {
      method: 'POST',
      path: '/search/{id}/renew',
      handler: function (request, reply) {
        server.methods.db.search.renew(request.params.id, (err, result) => {
          if (err) reply(err);
          reply()
        })
      }
    },
    {
      method: 'GET',
      path: '/searches/expire',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        server.methods.db.searches.expire((err, result) => {
          if (err) reply(err);
          if (result.changes) {
            result.changes.map((search) => {
              server.events.emit('expireSearch', search.new_val.id)
            })
          }
          reply(result)
        })
      }
    },
    {
      method: 'POST',
      path: '/search/{id}/found',
      handler: function (request, reply) {
        server.methods.db.search.setFound(request.params.id, request.auth.credentials.id, (err, result) => {
          if (err) reply(err)
          if (result.errors) {
            reply(result.errors).code(400)
          } else {
            server.events.emit('searchFound', request.params.id)
            reply()
          }
        })
      }
    },
    {
      method: 'POST',
      path: '/searches',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        let { location, query } = request.payload
        server.methods.db.searches.getActive(location, query, (err, results) => {
          if (err) reply(err);
          reply(results)
        })
      }
    },
    {
      method: 'POST',
      path: '/search/{id}/delete',
      handler: function (request, reply) {
        server.methods.db.search.delete(request.params.id, request.auth.credentials.id, (err, result) => {
          if (err) reply(err);
          if (result.errors) {
            reply(result.errors).code(400)
          } else {
            reply()
          }
        })
      }
    },
    {
      method: 'POST',
      path: '/search/{id}/discard',
      handler: function (request, reply) {
        server.methods.db.search.discard(request.params.id, request.payload.discard_id, (err, result) => {
          if (err) reply(err);
          if (result.errors) {
            reply(result.errors).code(400)
          } else {
            reply()
          }
        })
      }
    },
    {
      method: 'GET',
      path: '/search/{id}/matches',
      handler: function (request, reply) {
        server.methods.db.search.findMatches(request.params.id, (err, results) => {
          if (err) reply(err);
          reply(results)
        })
      }
    },
    {
      method: 'POST',
      path: '/search/add',
      handler: function (request, reply) {
        let search_data = request.payload.search_data
        search_data.user_id = request.auth.credentials.id
        server.methods.db.search.create(search_data, (err, result) => {
          if (err) reply(err);
          server.events.emit('newSearch', result)
          Wreck.put(`https://api.uploadcare.com/groups/${result.gallery.uuid}/storage/`, {
            headers: {
              'Authorization': `Uploadcare.Simple ${process.env.UPLOADCARE_PUBLIC_KEY}:${process.env.UPLOADCARE_SECRET_KEY}`
            }
          }, (err, res, payload) => {
          })
          reply(result)
        })
      }
    },
    {
      method: 'GET',
      path: '/search/{id}',
      config: {
        auth: false
      },
      handler: function (request, reply) {
        server.methods.db.search.getById(request.params.id, (err, result) => {
          if (err) reply(err);
          reply(result)
        })
      }
    }
  ])
}